const connection = require('./db');

const session = {
    authTokens: {},
    requireAuth: (req, res, next) => {
        connection.query('select * from tokens where token = ?', [req.cookies['AuthToken']], function (err, result) {
            if (err) {
                throw err;
            }
            if (result.length) {
                next();
            } else {
                // res.render('login', {
                //     message: 'Please login to continue',
                //     messageClass: 'alert-danger'
                // res.redirect('/todo');
                //res.json({logged: true});
                res.send(401);

            }
        });
    }
};

module.exports = session;
