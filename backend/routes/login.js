const express = require('express');
const router = express.Router();
const connection = require('../lib/db');
const hasher = require('../lib/hasher');

/* GET login. */
router.get('/', function (req, res, next) {
    // res.render('login');
    res.json({});
});

/* POST login. */
router.post('/', (req, res) => {
    console.log('post idzie');
    const { email, password } = req.body;
    const hashedPassword = hasher.getHashedPassword(password);

    connection.query('select email from users where email = ? and password = ?', [email, hashedPassword], function (err, result) {
        if (err) {
            throw err;
        }
        if (result.find((user => user.email === email)) !== undefined) {
            const authToken = hasher.generateAuthToken();
            connection.query('insert into tokens values (?, ?, ?)', [email, authToken, (new Date).getTime() + 48 * 60 * 60], function (err) {
                if (err) {
                    throw err;
                }

                //res.render('index', { message: 'Successfully logged in!', logged: true});
                res.json({ logged: true, authToken: authToken });
            });
        }
    });
});
module.exports = router;