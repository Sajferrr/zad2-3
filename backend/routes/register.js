const express = require('express');
const router = express.Router();
const connection = require('../lib/db');
const hasher = require('../lib/hasher');

/* GET /register */
router.get('/', function (req, res, next) {
    res.render('register', { title: 'Register' });
});

/* POST /register */
router.post('/', function (req, res, next) {
    const { email, firstName, password, confirmPassword } = req.body;
    if (password === confirmPassword) {
        connection.query('select * from users', [], function (err, result) {
            if (err) {
                throw err;
            }
            if (result.find(user => user.email === email) !== undefined) {
                res.json({
                    message: 'User already registered.'
                });
                return;
            }
            let hashedPassword = hasher.getHashedPassword(password);
            connection.query('insert into users ("email", "password") values (?, ?)', [hashedPassword, email], err => {
                if (err) {
                    throw err;
                }

            });
        });
    } else {
        res.json({
            message: 'Password does not match.'
        });
    }
});

module.exports = router;
