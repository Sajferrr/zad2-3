import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
// import './components/Index.css';
import Customers from './components/Customers';
import Todo from './components/Todo';
import Index from './components/Index';
import Login from './components/Login';
import Register from './components/register';
import { Redirect } from 'react-router-dom'
import ProtectedRoute from './components/protectedRoute';
class App extends Component {
  render() {
    const Serv = () => (
      <div>
        <Switch>
          <Route path='/customers' component={Customers} />
          <Route path='/register' component={Register} />
          <ProtectedRoute path='/todo' component={Todo} />
          <Route path='/login' component={Login} />
          <Route path='/' component={Index} />

        </Switch>
      </div>
    )

    return (
      <div className="App">
        <header className="App-header">

          {/* <Customers></Customers> */}
          <Switch>
            <Serv />
          </Switch>
          {/* <Todo></Todo> */}
        </header>

      </div>
    );
  }
}
export default App;
