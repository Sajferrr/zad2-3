import React, { Component } from 'react';
import './Todo.css';

class Todo extends Component {
  constructor() {
    super();
    this.state = {
      tasks: []
    }
  }
  componentDidMount() {
    fetch('/todo')
      .then(res => res.json())
      .then(tasks => this.setState({ tasks: tasks }));

  };
  render() {
    return (
      <div>
        <h2>Todo</h2>
        <ul>
          <li><a href="/">Main</a></li>
        </ul>
        <ul>

          {this.state.tasks.map(tasks => { return (<li>{tasks['task']}</li>) }

          )}
        </ul>


      </div>
    );
  }
}
export default Todo;
