class Auth {
    authenticated;
    constructor() {
        this.authenticated = false;
    }
    login() {
        this.authenticated = true;
    }
    logout() {
        this.authenticated = false;
    }
}
export default new Auth();