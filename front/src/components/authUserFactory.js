export default class AuthUserFactory {
    static auth = null;

    static getInstance() {
        if (AuthUserFactory.auth == null) {
            AuthUserFactory.auth = new Auth();
        }
    }
}