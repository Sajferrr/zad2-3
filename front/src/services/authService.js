import Cookies from 'js-cookie';

class AuthService {
    isUserAuthenticated() {
        var auth = JSON.parse(localStorage.getItem('auth'));
        var authToken = Cookies.get('AuthToken');
        return authToken != null && authToken != "" && auth != null && auth.authenticated
    }
    logout() {
        localStorage.removeItem('auth');
        Cookies.remove('AuthToken');
    }
}
export default new AuthService();